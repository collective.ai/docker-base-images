# Docker Base Images

_Docker base images with GPU support_
<br/>
<br/>

Setup
----
<br/>

```bash
$ git clone https://gitlab.com/collective.ai/docker-base-images
```

This repo requires Docker 20.10 or above

If you want to run the jupyter notebooks on a GPU you also need the latest version of [nvidia-container-toolkit](https://github.com/NVIDIA/nvidia-docker)

You can install nvidia-container-toolkit as follows:

```bash
$ distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
$ curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
$ curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

$ sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
$ sudo systemctl restart docker
```
<br/>

Usage
-----

You can do `make build`, `make run` and `make push` with the required envs `NAME` and, optionally, `VERSION`

### Examples

##### _jupyter-base_ build example
```bash
$ make build NAME=jupyter-base 
```
##### _python-cuda_ run example
```bash
$ make run NAME=python-cuda 
```
##### _python-cpu_ push example
```bash
$ make push NAME=python-cpu 
```

By default the version tag is set using the date version format.
If you wanna use a particular version you just need to set the environment variable VERSION.
for example:
```bash
$ make build NAME=jupyter-base VERSION=2021.06.30
```
