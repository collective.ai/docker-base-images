from setuptools import find_packages, setup

setup(
    name="collective-commons",
    packages=find_packages(),
    version="0.1.0",
    description="Common package & tools",
    author="collective.ai",
    author_email="team.collective.ai@gmail.com",
    install_requires=["tqdm==4.54.1", "typer==0.3.2"],
    scripts=[
        "collectiveai/scripts/jupyterlab-manager",
        "collectiveai/scripts/jupyterlab-setup",
        "collectiveai/scripts/jupyterlab-entrypoint",
        "collectiveai/scripts/install-requirements",
    ],
    package_data={"": ["*.yml", "*.yaml"]},
    include_package_data=True,
    classifiers=["Programming Language :: Python :: 3"],
)
