.PHONY: check-parameters login build run push
REPO = registry.gitlab.com
IMAGE = collective.ai/docker-base-images/$(NAME)

ifndef VERSION
VERSION = $(shell date +'%Y.%m.%d')
endif

check-parameters:
ifndef NAME
	$(error parameter NAME is required)
endif

login:
	docker login $(REPO)

build: check-parameters
	docker build -t $(REPO)/$(IMAGE):$(VERSION) -f ./$(NAME)/Dockerfile .
	docker tag $(REPO)/$(IMAGE):$(VERSION) $(REPO)/$(IMAGE):latest

run: check-parameters
	docker run -it --rm --privileged --network host $(REPO)/$(IMAGE):$(VERSION)

push: check-parameters build login
	docker push $(REPO)/$(IMAGE):$(VERSION)
	docker push $(REPO)/$(IMAGE):latest
